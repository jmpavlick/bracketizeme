﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

using NUnit.Framework;
using WikiTools.BracketizeMe;

namespace WikiToolsTests.BracketizeMe
{
	[TestFixture]
	class ParserTests
	{
		// tests
		[Test]
		public void AssertFunctionsInSqlFunctionListIgnored()
		{
			// don't bracketize SQL functions
			foreach (string function in SqlFunctionList.Instance.FunctionList)
			{
				Bracketizer b = new Bracketizer(function);
				Assert.IsTrue(b.BracketizedText == function);
			}
		}

		[Test]
		public void AssertSpaceInColumnNameIgnored()
		{
			string column = "[RX ID]";
			Bracketizer b = new Bracketizer(column);

			Assert.IsTrue(column == b.BracketizedText);
		}

		[Test]
		public void AssertKeywordsInSqlKeywordListIgnored()
		{
			// don't bracketize SQL keywords
			foreach (string keyword in SqlKeywordList.Instance.KeywordList)
			{
				Bracketizer b = new Bracketizer(keyword);
				Assert.IsTrue(b.BracketizedText == keyword);
			}
		}

		[Test]
		public void AssertKeywordsInSqlKeywordListUppercased()
		{
			// uppercase SQL keywords to meet DSP standards
			string s = "select * from DataPool.dbo.tblENAVRX";
			Bracketizer b = new Bracketizer(s);
			Console.WriteLine(b.BracketizedText);
			Assert.IsTrue(b.BracketizedText == "SELECT * FROM [DataPool].[dbo].[tblENAVRX]");
		}

		[Test]
		public void AssertVariablesIgnored()
		{
			// don't bracketize @variables
			string[] vl = { "@StartDate", "@EndDate", "@DrugName", "@ProcessName" };
			List<string> variableList = vl.ToList();
			foreach (string variable in variableList)
			{
				Bracketizer b = new Bracketizer(variable);
				Console.WriteLine(b.BracketizedText);
				Assert.IsTrue(b.BracketizedText == variable);
			}

			string s = "@DrugName DrugName @DrugName";
			Bracketizer x = new Bracketizer(s);

			Console.WriteLine(x.BracketizedText);

			Assert.IsTrue(x.BracketizedText == "@DrugName [DrugName] @DrugName");
		}

		[Test]
		public void AssertTempTablesBracketized()
		{
			string s = "#tempTable tempTable #tempTable";
			Bracketizer b = new Bracketizer(s);
			Console.WriteLine(b.BracketizedText);
			Assert.IsTrue(b.BracketizedText == "[#tempTable] [tempTable] [#tempTable]");
		}

		[Test]
		public void AssertPreBracketizedWordsNotDoubleBracketed()
		{
			// none of this: [[erx]]
			string[] bl = { "[erx]", "[DataPool]", "[dbo]", "SELECT * FROM [dbo].[tblIbrutinibDispenseReport]" };
			List<string> bracketList = bl.ToList();
			foreach (string bracketedWord in bracketList)
			{
				Bracketizer b = new Bracketizer(bracketedWord);
				Assert.IsTrue(b.BracketizedText == bracketedWord);
			}
		}

		[Test]
		public void AssertAllOccurancesBracketed()
		{
			string s = "dbo [dbo] dbo [dbo] dbo";
			Bracketizer b = new Bracketizer(s);
			Assert.IsTrue(b.BracketizedText == "[dbo] [dbo] [dbo] [dbo] [dbo]");
		}

		[Test]
		public void AssertCommentsIgnored()
		{
			string singleLineComment = "-- this is a single-line comment";
			string multiLineComment = "/* this is a\r\nmulti-line\r\ncomment */";
			string bothTypesOfComment = "-- this is a single-line comment\r\n/*multi\r\nline\r\ncomment*/";

			Bracketizer b1 = new Bracketizer(singleLineComment);
			Bracketizer b2 = new Bracketizer(multiLineComment);
			Bracketizer b3 = new Bracketizer(bothTypesOfComment);

			Console.WriteLine(b1.BracketizedText);
			Console.WriteLine(b2.BracketizedText);
			Console.WriteLine(b3.BracketizedText);

			Assert.IsTrue(b1.BracketizedText == singleLineComment);
			Assert.IsTrue(b2.BracketizedText == multiLineComment);
			Assert.IsTrue(b3.BracketizedText == bothTypesOfComment);
		}

		[Test]
		public void AssertRemoveSpecial()
		{
			// special cases!
			string datepartTest = "DATEPART(dd, erx.ShipDate)";
			Bracketizer bDatepartTest = new Bracketizer(datepartTest);
			Assert.IsTrue(bDatepartTest.BracketizedText == "DATEPART(dd, [erx].[ShipDate])");
		}

		[Test]
		public void AssertDigitsIgnored()
		{
			string integerAsString = "9999";
			string decimalAsString = "99.99";

			Bracketizer b1 = new Bracketizer(integerAsString);
			Bracketizer b2 = new Bracketizer(decimalAsString);

			Assert.IsTrue(b1.BracketizedText == integerAsString);
			Assert.IsTrue(b2.BracketizedText == decimalAsString);
		}

		[Test]
		public void AssertWordsWithNumbersBracketed()
		{
			string s = "addr1 rx30sp RX30FirstName DIAGNOSIS01";
			Bracketizer b = new Bracketizer(s);
			Assert.IsTrue(b.BracketizedText == @"[addr1] [rx30sp] [RX30FirstName] [DIAGNOSIS01]");
		}

		[Test]
		public void AssertCommasUsedAsSplitCharacter()
		{
			string s = "29,'2349',erx.RXID";
			Bracketizer b = new Bracketizer(s);
			Assert.IsTrue(b.BracketizedText == @"29,'2349',[erx].[RXID]");
		}

		[Test]
		public void AssertDotOperatorUsedAsSplitCharacter()
		{
			string s = "DataPool.dbo.tblENAVRX";
			Bracketizer b = new Bracketizer(s);
			Assert.IsTrue(b.BracketizedText == "[DataPool].[dbo].[tblENAVRX]");
		}

		[Test]
		public void AssertParenthesisIgnored()
		{
			string s = @"SomeFunctionName(29,'2349',erx.RXID)";
			Bracketizer b = new Bracketizer(s);
			Assert.IsTrue(b.BracketizedText == @"[SomeFunctionName](29,'2349',[erx].[RXID])");
		}

		[Test]
		public void AssertColonIgnored()
		{
			string s = "CASE:\r\nWHEN erx.RXID = dsd.RXID\r\nTHEN erx.RXID\r\nELSE ''\r\nEND AS RXID";
			string x = "CASE:\r\nWHEN [erx].[RXID] = [dsd].[RXID]\r\nTHEN [erx].[RXID]\r\nELSE ''\r\nEND AS [RXID]";
			Bracketizer b = new Bracketizer(s);
			Console.WriteLine("AssertColonIgnored:");
			Console.WriteLine(s);
			Console.WriteLine(b.BracketizedText);
			Console.WriteLine();

			Assert.IsTrue(b.BracketizedText == x);
		}

		[Test]
		public void AssertPreBracketizedTempTableNotDoubleBracketized()
		{
			string s = @"[#tempTable]";
			Bracketizer b = new Bracketizer(s);
			Console.WriteLine(b.BracketizedText);
			Assert.IsTrue(b.BracketizedText == s);
		}

		[Test]
		public void AssertStringLiteralsWithWhitespaceNotPartiallyBracketized()
		{
			string s = @"'Diplomat Specialty Pharmacy'";
			Bracketizer b = new Bracketizer(s);
			Assert.IsTrue(b.BracketizedText == @"'Diplomat Specialty Pharmacy'");
		}

		[Test] public void AssertInlineSqlNotBracketed()
		{
			string s = @"N'dbo.tblENAVRX'";
			Bracketizer b = new Bracketizer(s);

			Console.WriteLine("AssertInlineSqlNotBracketed");
			Console.WriteLine(b.BracketizedText);
			Console.WriteLine();

			Assert.IsTrue(b.BracketizedText == @"N'dbo.tblENAVRX'");
		}

		[Test]
		public void AssertSimpleProcedureIsParsedCorrectly()
		{
			StringBuilder input = new StringBuilder();
			input.AppendLine("-- this is a single-line comment");
			input.AppendLine("/* this is a");
			input.AppendLine("\tmulti-line");
			input.AppendLine("comment */");
			input.AppendLine("SELECT -- inline single-line comment");
			input.AppendLine("\terx.RXID,");
			input.AppendLine("\terx.DrugID,");
			input.AppendLine("\terx.DoctorID,");
			input.AppendLine("\terx.PharmacyName,");
			input.AppendLine("\tSomeFunction(ISNULL(erx.RXID,''), erx.RXCost + dsd.ShippingFees),");
			input.AppendLine("\terx.NDC,");
			input.AppendLine("\terx.PatientID,");
			input.AppendLine("\tCASE:\r\t\tWHEN erx.RXID = dsd.RXID\r\t\tTHEN erx.RXID\r\t\tELSE ''\r\tEND AS RXID");
			input.AppendLine("FROM DataPool.dbo.tblENAVRX AS erx");
			input.AppendLine("INNER JOIN DataPool.dbo.tblDRUG AS drug");
			input.AppendLine("\tON drug.DRUGID == erx.DrugID");

			StringBuilder expectedOutput = new StringBuilder();
			expectedOutput.AppendLine("-- this is a single-line comment");
			expectedOutput.AppendLine("/* this is a");
			expectedOutput.AppendLine("\tmulti-line");
			expectedOutput.AppendLine("comment */");
			expectedOutput.AppendLine("SELECT -- inline single-line comment");
			expectedOutput.AppendLine("\t[erx].[RXID],");
			expectedOutput.AppendLine("\t[erx].[DrugID],");
			expectedOutput.AppendLine("\t[erx].[DoctorID],");
			expectedOutput.AppendLine("\t[erx].[PharmacyName],");
			expectedOutput.AppendLine("\t[SomeFunction](ISNULL([erx].[RXID],''), [erx].[RXCost] + [dsd].[ShippingFees]),");
			expectedOutput.AppendLine("\t[erx].[NDC],");
			expectedOutput.AppendLine("\t[erx].[PatientID],");
			expectedOutput.AppendLine("\tCASE:\r\t\tWHEN [erx].[RXID] = [dsd].[RXID]\r\t\tTHEN [erx].[RXID]\r\t\tELSE ''\r\tEND AS [RXID]");
			expectedOutput.AppendLine("FROM [DataPool].[dbo].[tblENAVRX] AS [erx]");
			expectedOutput.AppendLine("INNER JOIN [DataPool].[dbo].[tblDRUG] AS [drug]");
			expectedOutput.AppendLine("\tON [drug].[DRUGID] == [erx].[DrugID]");

			Bracketizer inputBracketizer = new Bracketizer(input.ToString());
			Bracketizer expectedOutputBracketizer = new Bracketizer(expectedOutput.ToString());

			// the unbracketized input should equal the expected output
			Assert.IsTrue(expectedOutput.ToString() == inputBracketizer.BracketizedText);
			// and the bracketized expected output should equal the expected output
			Assert.IsTrue(expectedOutput.ToString() == expectedOutputBracketizer.BracketizedText);
		}

		[Test]
		public void AssertExtraNewlineNotAdded()
		{
			int lineCountBeforeBracketize = 0;
			int lineCountAfterBracketize = 0;

			StringBuilder sb = new StringBuilder();
			sb.AppendLine("SELECT");
			sb.AppendLine("\t*");
			sb.AppendLine();
			sb.AppendLine("FROM XTable AS x");
			sb.AppendLine();

			lineCountBeforeBracketize = sb.ToString().Split('\n').Count();

			Bracketizer b = new Bracketizer(sb.ToString());
			lineCountAfterBracketize = b.BracketizedText.Split('\n').Count();

			Console.WriteLine("Before: {0}\nAfter: {1}", lineCountBeforeBracketize, lineCountAfterBracketize);

			Assert.IsTrue(lineCountBeforeBracketize == lineCountAfterBracketize);
		}
	}
}
