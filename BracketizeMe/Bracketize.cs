﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;

namespace WikiTools.BracketizeMe
{
	public class Bracketizer
	{
		// private properties
		private List<string> _inputLines;
		private StringBuilder _bracketizedText;
		private bool _inMultiLineComment;
		/// regex patterns
		private string _inputSplitPattern;
		private string _singleLineCommentPattern;
		private string _multiLineCommentBeginPattern;
		private string _multiLineCommentEndPattern;
		private string _lineSplitPattern;
		private string _digitsPattern;
		private string _dateFunctionPattern;
		private string _stringLiteralPattern;
		private string _tempTablePattern;
		private string _alreadyBracketizedPattern;

		// public properties
		public string BracketizedText
		{
			get { return _bracketizedText.ToString(); }
		}

		// constructor
		public Bracketizer(string input)
		{
			_inMultiLineComment = false;
			_bracketizedText = new StringBuilder();
			// set up regex patterns
			_inputSplitPattern = @"\r\n";
			_singleLineCommentPattern = @"[-]{2}.*$";
			_multiLineCommentBeginPattern = @"\/\*.*$";
			_multiLineCommentEndPattern = @"^.*\*\/";
			_lineSplitPattern = @"[^a-zA-Z0-9@_#]";
			_digitsPattern = @"\b\d+\.?\d?\b";
			_dateFunctionPattern = @"DATE[a-zA-Z]+\([a-zA-Z]{1,2}";
			_stringLiteralPattern = @"'[^']*'";
			_tempTablePattern = @"\[?#\w+\]?";
			_alreadyBracketizedPattern = @"\[[a-zA-Z0-9\s]+\]";

			// split lines and create _inputLines list
			_inputLines = Regex.Split(input, _inputSplitPattern).ToList();

			// process text
			//foreach (string line in _inputLines)
			for (int i = 0; i < _inputLines.Count; i++)
			{
				string tempLine = _inputLines[i];
				string masterLine = _inputLines[i];

				if (_inMultiLineComment == false)
				{
					// ignore multi-line comments
					if (Regex.IsMatch(tempLine, _multiLineCommentBeginPattern))
					{
						_inMultiLineComment = true;
						tempLine = Regex.Replace(tempLine, _multiLineCommentBeginPattern, "");
					}
				}
				else
				{
					if (Regex.IsMatch(tempLine, _multiLineCommentEndPattern))
					{
						tempLine = Regex.Replace(tempLine, _multiLineCommentEndPattern, "");
						_inMultiLineComment = false;
					}
					else
					{
						_bracketizedText.AppendLine(tempLine);
						continue;
					}
				}

				// ignore single-line comments
				if (Regex.IsMatch(tempLine, _singleLineCommentPattern))
				{
					tempLine = Regex.Replace(tempLine, _singleLineCommentPattern, "");
				}

				// remove digits
				tempLine = Regex.Replace(tempLine, _digitsPattern, "");

				// remove date function stuff
				tempLine = Regex.Replace(tempLine, _dateFunctionPattern, "", RegexOptions.IgnoreCase);

				// remove string literals
				tempLine = Regex.Replace(tempLine, _stringLiteralPattern, "");

				// remove anything already in brackets
				tempLine = Regex.Replace(tempLine, _alreadyBracketizedPattern, "");

				// bracketize
				HashSet<string> uniqueWords = new HashSet<string>(Regex.Split(tempLine, _lineSplitPattern).ToList());
				HashSet<string> variableSet = new HashSet<string>();
				HashSet<string> tempTableSet = new HashSet<string>();

				foreach (string word in uniqueWords)
				{
					// ignore empty strings
					if (String.IsNullOrEmpty(word))
					{
						continue;
					}

					// UPPERCASE SQL keywords
					if (SqlKeywordList.Instance.KeywordList.Contains(word.ToUpper()))
					{
						masterLine = Regex.Replace(masterLine, word, word.ToUpper());
						continue;
					}

					// ignore SQL functions
					if (SqlFunctionList.Instance.FunctionList.Contains(word.ToUpper()))
					{
						continue;
					}

					// ignore @variables
					if (word[0] == '@')
					{
						variableSet.Add(word);
						continue;
					}

					// ignore #tempTables
					if (Regex.IsMatch(word, _tempTablePattern))
					{
						tempTableSet.Add(word);
						continue;
					}

					string bracketizedWord = "[" + word + "]";
					masterLine = Regex.Replace(masterLine, @"\[?\b" + Regex.Escape(word) + @"\b\]?", bracketizedWord);
				}

				// masterLine now contains the bracketized version of itself
				// fix variables
				foreach (string variable in variableSet)
				{
					masterLine = Regex.Replace(masterLine, @"[@]\[\b" + variable.Substring(1, variable.Length - 1) + @"\b\]", variable);
				}

				// fix tempTables
				foreach (string tempTable in tempTableSet)
				{
					string bracketizedTempTable = "[" + tempTable + "]";
					masterLine = Regex.Replace(masterLine, @"\[?[#]\[?\b" + tempTable.Substring(1, tempTable.Length - 1) + @"\b\]?", bracketizedTempTable);
				}

				_bracketizedText.Append(masterLine);

				// if we're not on the last line, append a newline
				if ((i + 1) != _inputLines.Count)
				{
					_bracketizedText.AppendLine();
				}
			}
		}
	}
}
